package parse

import (
	"strings"
	"errors"
)

type Parseinfo struct {
	Background bool
	Cmd        string
	Varlist	   []string
	Infile     string
	Outfile    string
}

// since the language of commands is so simple, I just deal with it like this
// it sure takes a lot less code in go
func Parseline(command string) (Parseinfo, error) {
	var info Parseinfo
	t := strings.Split(strings.TrimSpace(command), " ") // split the command up into a slice of strings
	if t[0] == "" {
		return info, errors.New("parse: empty command")
	}
	info.Cmd = command
	info.Varlist = append(info.Varlist, t[0])
	// set up a slice of command line args
	// ignore everything after the first shell reserved rune (a rune is a UTF-8 char)
	var i int
	for i = 1; i < len(t); i++ {
		if t[i] != "<" && t[i] != ">" && t[i] != "&" {
			info.Varlist = append(info.Varlist, t[i])
		} else {
			break
		}
	}

	for {
		if i >= len(t) {
			return info, nil
		}
		switch t[i] {
		case ">":
			if i+1 < len(t) {
				info.Outfile = t[i+1]
				i++
			}
		case "<":
			if i+1 < len(t) {
				info.Infile = t[i+1]
				i++
			}
		case "&":
			info.Background = true
		default:
			return info, errors.New("parse: malformed expression")
		}
		i++
	}
	info.Background = (t[len(t) - 1] == "&")
	return info, nil
}
