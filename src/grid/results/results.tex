\documentclass{article}

\author{Ethan Miller\\
	$\texttt{millere@clarkson.edu}$}
\title{Results: Grid Locking Granularities}
\date{\today}

\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage[hidelinks]{hyperref}
\usepackage{listings}
\lstset{language=c}

\begin{document}

\maketitle

\section{Locking and Deadlock Prevention Scheme}
\subsection{Granularity: None}
With no locking, there can be no locking schema and no deadlock.

This (except in the single thread case) leads to data integrity violations, as multiple threads are playing around and moving data that other threads are using.
This does not occur in the single thread case because there are no other threads to change things while the thread is sleeping.

\subsection{Granularity: Grid}
For locking the grid, a single \verb+pthread_mutex_t+ is used.
There is only a single resource available to lock, and thus no deadlock can occur.

\subsection{Granularity: Row}
Row granularity is achieved by having a \verb+pthread_mutex_t+ for each row.
In order to modify a cell in a row, that thread must have the lock for that row.
To prevent deadlock, the resources are ordered such that for any two rows $n_1$ and $n_2$, the row that is locked first is $\min(n_1, n_2)$.
This prevents deadlock by preventing cycles in the resource graph.

\subsection{Granularity: Cell}
Cell granularity follows largely the same strategy as is used in row granularity.
There is a \verb+pthread_mutex_t[][]+ that holds a lock for each cell.
In order to modify a cell, a thread must have the lock for that cell.

Ordering of the cell mutexes is done by calculating $10*\texttt{MAXGRIDSIZE}*\texttt{row}+\texttt{column}$.
Because $\texttt{column} < 10*\texttt{MAXGRIDSIZE}$, this number is guaranteed to be unique for each $(\texttt{row}, \texttt{column})$.
Thus, by comparing this number for any two given rows and columns, a consistent ordering is found.

\section{Analysis}

\subsection{Effect of grid size}
With grid locking, the time taken is entirely a function of the number of threads (namely, 20 seconds per thread).
This is because of each swap taking 1 second, each thread performing 20 swaps, and while any thread is performing a swap, all other threads are waiting.
This can be clearly seen in figure~\ref{fig:gg}.

With row and cell level locking, however, a thread is able to proceed as long as the row or cell (respectively) it requires is unlocked.
Thus, as grid size increases, the number of collisions between threads wanting the same resources decreases logarithmically, as seen in figures~\ref{fig:rg} and~\ref{fig:cg}.
Namely, the larger the grid, the faster the same number of threads can act on it.

\subsection{Effect of the number of threads}
With grid level locking, as long as one thread is doing a swap, the rest are sleeping.
Thus, with grid level locking, the time required is the same as the time that would be required if the swaps were done in a single thread.
This implies that in this case, by varying the number of threads, we are actually varying the number of swaps to be done.
Each swap takes 1 second. Thus, time required is $O(n)$ as can be seen in figure~\ref{fig:gt}.

With row and cell level locking, time taken is strongly reduced by allowing multiple threads into the grid at the same time.
As the number of threads increases, so too does the number of collisions that occur between threads wanting the same resources.
Because each additional thread brings with it an additional 20 swaps, increasing threads increases the amount of work that needs to be done.
This increased work combined with increased collisions brings a linear running time as can be seen in figures~\ref{fig:rt} and~\ref{fig:ct}.\footnote{I believe that increasing the amount of work to be done with each additional thread is a flaw in the sample code, because for most cases where threading is appropriate, the amount of work to do does not vary with the number of threads used. To fix this, instead the number of swaps done should be constant and each thread should do $\frac{1}{\text{threads}}$ of those swaps. That number ideally would be 2520 (to be evenly divisible by all possible number of threads), but the timings may have to be adjusted.}

\subsection{Effect of granularity of locking}
The levels of granularity can be ordered by the amount of data they lock, with grid locking the most data and cell locking the least.
In every case, the amount of time taken is directly proportional to the amount of data locked.
This is due to that, as the amount of data locked increases, so too does the number of collisions between threads wanting the same resources.

Namely, we see that the finer grained the locking is, the faster the program is able to run.
The effects of fine locking granularity are largest on a smaller grid with a larger number of threads.
Fine granularity does, however, come with a cost: more memory is used to store each of the locks used.

\subsection{Effect of $\texttt{sleep(1)}$}
The critical section of swapping consists of the following:
\begin{lstlisting}
int temp = grid[row1][column1];
grid[row1][column1] = grid[row2][column2];
grid[row1][column1] = temp;
\end{lstlisting}

This takes very little time as written, and can be optimized to 3 cycles (using bitwise xor three times).
This has two main effects on the experiment:
\begin{itemize}
	\item The probability of a thread being swapped out in the middle of the swap is very unlikely, leading to fewer collisions.
	\item The time taken is much less, making accurate timing more difficult.
\end{itemize}

By adding the $\texttt{sleep(1)}$ between the assignment of temp and the fixing of the internal state, we increase the likelyhood of another thread coming in and (without locking) causing loss of data integrity, as well as making the measurement of time elapsed more easily read.

\begin{figure}[p]
\centering
\includegraphics{none-constant-grid}
\caption{With no granularity, constant grid size and varied threads.}
\label{fig:ng}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics{none-constant-threads}
\caption{With no locking, constant number of threads and varied grid size.}
\label{fig:nt}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics{grid-constant-grid}
\caption{With grid level locking, constant grid size and varied threads.}
\label{fig:gg}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics{grid-constant-threads}
\caption{With grid level locking, constant number of threads and varied grid size.}
\label{fig:gt}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics{row-constant-grid}
\caption{With row level locking, constant grid size and varied threads.}
\label{fig:rg}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics{row-constant-threads}
\caption{With row level locking, constant number of threads and varied grid size.}
\label{fig:rt}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics{cell-constant-grid}
\caption{With cell level locking, constant grid size and varied threads.}
\label{fig:cg}
\end{figure}

\begin{figure}[p]
\centering
\includegraphics{cell-constant-threads}
\caption{With cell level locking, constant number of threads and varied grid size.}
\label{fig:ct}
\end{figure}

\end{document}
