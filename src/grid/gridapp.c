#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#ifdef __linux__
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#elif defined __WIN32
#include <windows.h>
#endif

#ifdef __WIN32
#warning "Testing on Windows is not as complete as testing on Linux. Please test on Linux for an accurate idea of code correctness"
#endif

#define MAXGRIDSIZE 	10
#define MAXTHREADS	1000
#define NO_SWAPS	20

extern int errno;

typedef enum {GRID, ROW, CELL, NONE} grain_type;

int gridsize = 0;
int grid[MAXGRIDSIZE][MAXGRIDSIZE];
int threads_left = 0;
#ifdef __linux__
pthread_mutex_t grid_mutex;
pthread_mutex_t thread_count_mutex;
pthread_mutex_t row_mutex[MAXGRIDSIZE];
pthread_mutex_t cell_mutex[MAXGRIDSIZE][MAXGRIDSIZE];
#elif defined __WIN32
HANDLE grid_mutex;
HANDLE thread_count_mutex;
HANDLE row_mutex[MAXGRIDSIZE];
HANDLE cell_mutex[MAXGRIDSIZE];
#endif

time_t start_t, end_t;

int print_grid(int grid[MAXGRIDSIZE][MAXGRIDSIZE], int gridsize)
{
	int i;
	int j;

	for (i = 0; i < gridsize; i++) {
		for (j = 0; j < gridsize; j++) {
			fprintf(stdout, "%d\t", grid[i][j]);
		}
		fprintf(stdout, "\n");
	}
	return 0;
}


long init_grid(int grid[MAXGRIDSIZE][MAXGRIDSIZE], int gridsize)
{
	int i;
	int j;
	long sum = 0;
	int temp = 0;

	srand(time(NULL));

	for (i = 0; i < gridsize; i++) {
		for (j = 0; j < gridsize; j++) {
			temp = rand() % 100;
			grid[i][j] = temp;
			sum = sum + temp;
		}
	}

	return sum;

}

long sum_grid(int grid[MAXGRIDSIZE][MAXGRIDSIZE], int gridsize)
{
	int i, j;
	long sum = 0;

	for (i = 0; i < gridsize; i++) {
		for (j = 0; j < gridsize; j++) {
			sum += grid[i][j];
		}
	}
	return sum;

}
#ifdef __linux__
void *do_swaps(void *args)
#elif defined __WIN32
DWORD WINAPI do_swaps(void *args)
#endif
{
	int i, row1, column1, row2, column2;
	int weight1, weight2;
	int temp;
	grain_type *gran_type = (grain_type*)args;

	threads_left++;

	for(i=0; i<NO_SWAPS; i++) {
		row1 = rand() % gridsize;
		column1 = rand() % gridsize;
		row2 = rand() % gridsize;
		column2 = rand() % gridsize;

		// I use weight to order the resources for cell-level granularity
		// each weight is basically cat(row,column) with a possible padding 0
		weight1 = 10*MAXGRIDSIZE*row1 + column1;
		weight2 = 10*MAXGRIDSIZE*row2 + column2;

		if (*gran_type == ROW) {
			// get smaller row first - order resources to prevent deadlock
#ifdef __linux__
			pthread_mutex_lock(&row_mutex[(row1 > row2)? row2 : row1]);
			if (row1 != row2) {
				pthread_mutex_lock(&row_mutex[(row1 < row2)? row2 : row1]);
			}
#elif defined __WIN32
			WaitForSingleObject(row_mutex[(row1 > row2)? row2 : row1]);
			if (row1 != row2) {
				WaitForSingleObject(row_mutex[(row1 < row2)? row2 : row1]);
			}
#endif
		} else if (*gran_type == CELL) {
			if (weight1 < weight2) {
#ifdef __linux__
				pthread_mutex_lock(&cell_mutex[row1][column1]);
				pthread_mutex_lock(&cell_mutex[row2][column2]);
#elif defined __WIN32
				WaitForSingleObject(cell_mutex[row1][column1]);
				WaitForSingleObject(cell_mutex[row2][column2]);
#endif
			} else if ( weight1 > weight2) {
#ifdef __linux__
				pthread_mutex_lock(&cell_mutex[row2][column2]);
				pthread_mutex_lock(&cell_mutex[row1][column1]);
#elif defined __WIN32
				WaitForSingleObject(cell_mutex[row2][column2]);
				WaitForSingleObject(cell_mutex[row1][column1]);
#endif
			} else { // weights are equal iff row1==row2 && column1==column2
#ifdef __linux__
				pthread_mutex_lock(&cell_mutex[row1][column1]);
#elif defined __WIN32
				WaitForSingleObject(cell_mutex[row1][column1]);
#endif
			}
		} else if (*gran_type == GRID) {
#ifdef __linux__
			pthread_mutex_lock(&grid_mutex);
#elif defined __WIN32
			WaitForSingleObject(grid_mutex);
#endif
		}

		temp = grid[row1][column1];
#ifdef __linux__
		sleep(1);
#elif defined __WIN32
		Sleep(1000);
#endif
		grid[row1][column1]=grid[row2][column2];
		grid[row2][column2]=temp;

		if (*gran_type == ROW) {
			// order for freeing doesn't matter
#ifdef __linux__
			pthread_mutex_unlock(&row_mutex[row1]);
			if (row1 != row2) {
				pthread_mutex_unlock(&row_mutex[row2]);
			}
#elif defined __WIN32
			ReleaseMutex(row_mutex[row1]);
			if (row1 != row2) {
				ReleaseMutex(row_mutex[row2]);
			}
#endif
		} else if (*gran_type == CELL) {
#ifdef __linux__
			pthread_mutex_unlock(&cell_mutex[row1][column1]);
			if (weight1 != weight2) {
				pthread_mutex_unlock(&cell_mutex[row2][column2]);
			}
#elif defined __WIN32
			ReleaseMutex(cell_mutex[row1][column1]);
			if (weight1 != weight2) {
				ReleaseMutex(cell_mutex[row2][column2]);
			}
#endif
		} else if (*gran_type == GRID) {
#ifdef __linux__
			pthread_mutex_unlock(&grid_mutex);
#elif defined __WIN32
			ReleaseMutex(grid_mutex);
#endif
		}
	}

	/* does this need protection? */
	// yes. yes it does - exm
#ifdef __linux__
	pthread_mutex_lock(&thread_count_mutex);
#elif defined __WIN32
	WaitForSingleObject(thread_count_mutex);
#endif
	threads_left--;
	if (threads_left == 0) { /* if this is last thread to finish*/
		time(&end_t);         /* record the end time*/
	}
#ifdef __linux__
	pthread_mutex_unlock(&thread_count_mutex);
#elif defined __WIN32
	ReleaseMutex(thread_count_mutex);
#endif
	return NULL;
}

int main(int argc, char **argv)
{
	int nthreads = 0;
#ifdef __linux__
	pthread_t threads[MAXTHREADS];
#elif defined __WIN32
	HANDLE threads[MAXTHREADS];
#endif
	grain_type rowGranularity = NONE;
	long initSum = 0, finalSum = 0;
	int i, j;

	if (argc > 3) {
		gridsize = atoi(argv[1]);
		if (gridsize > MAXGRIDSIZE || gridsize < 1) {
			printf("Grid size must be between 1 and 10.\n");
			return(1);
		}
		nthreads = atoi(argv[2]);
		if (nthreads < 1 || nthreads > MAXTHREADS) {
			printf("Number of threads must be between 1 and 1000.");
			return(1);
		}

		if (argv[3][1] == 'r' || argv[3][1] == 'R') {
			rowGranularity = ROW;
		}
		if (argv[3][1] == 'c' || argv[3][1] == 'C') {
			rowGranularity = CELL;
		}
		if (argv[3][1] == 'g' || argv[3][1] == 'G') {
			rowGranularity = GRID;
		}

	} else {
		printf("Format:  gridapp gridSize numThreads -cell\n");
		printf("         gridapp gridSize numThreads -row\n");
		printf("         gridapp gridSize numThreads -grid\n");
		printf("         gridapp gridSize numThreads -none\n");
		return(1);
	}

	// initialize all of my mutexes and semaphores.
#ifdef __linux__
	pthread_mutex_init(&grid_mutex, NULL);
	pthread_mutex_init(&thread_count_mutex, NULL);
	for (i = 0; i < MAXGRIDSIZE; ++i) {
		pthread_mutex_init(&row_mutex[i], NULL);
		for (j = 0; j < MAXGRIDSIZE; ++j) {
			pthread_mutex_init(&cell_mutex[i][j], NULL);
		}
	}
#elif defined __WIN32
	grid_mutex = CreateMutex(NULL, FALSE, NULL);
	thread_count_mutex = CreateMutex(NULL, FALSE, NULL);
	for (i = 0; i < MAXGRIDSIZE; ++i) {
		row_mutex[i] = CreateMutex(NULL, FALSE, NULL);
		for (j = 0; j < MAXGRIDSIZE; ++j) {
			cell_mutex[i][j] = CreateMutex(NULL, FALSE, NULL);
		}
	}
#endif

	printf("Initial Grid:\n\n");
	initSum =  init_grid(grid, gridsize);
	print_grid(grid, gridsize);
	printf("\nInitial Sum:  %ld\n", initSum);
	printf("Executing threads...\n");

	/* better to seed the random number generator outside
	   of do swaps or all threads will start with same
	   choice
	   */
	srand(time(NULL));

	time(&start_t);
	for (i = 0; i < nthreads; i++) {
#ifdef __linux__
		if (pthread_create(&(threads[i]), NULL, do_swaps, (void *)(&rowGranularity)) != 0) {
			perror("thread creation failed:");
			exit(-1);
		}
#elif defined __WIN32
		thread[i] = CreateThread(NULL, 0, do_swaps, (void *)(&rowGranularity), 0, NULL);
		if (!thread[i]) {
			fprintf(stderr, "thread creation failed");
			exit(-1);
		}
#endif
	}

	for (i = 0; i < nthreads; i++) {
#ifdef __linux__
		pthread_detach(threads[i]);
#elif defined __WIN32

#endif
	}

#ifdef __linux__
	pthread_mutex_destroy(&grid_mutex);
	for (i = 0; i < MAXGRIDSIZE; ++i) {
		pthread_mutex_destroy(&row_mutex[i]);
		for (j = 0; j < MAXGRIDSIZE; ++j) {
			pthread_mutex_destroy(&cell_mutex[i][j]);
		}
	}
	pthread_mutex_destroy(&thread_count_mutex);
#elif defined __WIN32
	CloseHandle(grid_mutex);
	for (i = 0; i < MAXGRIDSIZE; ++i) {
		CloseHandle(row_mutex[i]);
		for (j = 0; j < MAXGRIDSIZE; ++j) {
			CloseHandle(cell_mutex[i][j]);
		}
	}
#endif

	while (1) {
		sleep(2);
		if (threads_left == 0) {
			fprintf(stdout, "\nFinal Grid:\n\n");
			print_grid(grid, gridsize);
			finalSum = sum_grid(grid, gridsize);
			fprintf(stdout, "\n\nFinal Sum:  %ld\n", finalSum);
			if (initSum != finalSum) {
				fprintf(stdout,"DATA INTEGRITY VIOLATION!!!!!\n");
			} else {
				fprintf(stdout,"DATA INTEGRITY MAINTAINED!!!!!\n");
			}
			fprintf(stdout, "Secs elapsed:  %g\n", difftime(end_t, start_t));

			exit(0);
		}
	}

	return(0);

}
