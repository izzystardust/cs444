/************************
  CS444/544 Operating System

  Week 07: Solving the producer/consumer problem

  Locking scheme description:
  Three semaphores, described in readme




Name: Ethan Miller

Clarkson University  SP2014
 **************************/ 

/***** Includes *****/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

/***** Defines *****/
#define PRODUCER_SLEEP_S	1		//in seconds
#define CONSUMER_SLEEP_S	1
#define QUEUESIZE 			5
#define LOOP 				10


/***** Function prototypes *****/
void *producer (void *args);
void *consumer (void *args);

/***** Queue struct *****/
typedef struct {
	int buf[QUEUESIZE];
	long head, tail;
	int full, empty;  //you may need or may not

	/*Declare the locks here */
} queue;

/***** Queue function prototypes  *****/
queue *queueInit (void);
void queueDelete (queue *q);
void queueAdd (queue *q, int in);
void queueDel (queue *q, int *out);

sem_t full;
sem_t empty;
sem_t mutex;

/***** main *****/
int main ()
{
	queue *fifo;
	sem_init(&full, 0, 0);
	sem_init(&empty, 0, QUEUESIZE);
	sem_init(&mutex, 0, 1);

	int i;

	//for Consumer's random coming
	unsigned int iseed = (unsigned int)time(NULL);
	srand(iseed);

	/******one producer and multiple consumer********/
	pthread_t pro, con[LOOP];

	fifo = queueInit ();
	if (fifo ==  NULL) {
		fprintf (stderr, "main: Queue Init failed.\n");
		exit (1);
	}

	pthread_create (&pro, NULL, producer, fifo);
	for(i=0; i<LOOP; i++)
	{
		pthread_create (&con[i], NULL, consumer, fifo);
	}



	pthread_join (pro, NULL);
	for(i=0; i<LOOP; i++)
	{
		pthread_join (con[i], NULL);	
	}
	queueDelete (fifo);

	return 0;
}

/***** producer *****/
void *producer (void *q)
{

	queue *fifo;
	int i;

	fifo = (queue *)q;


	for (i = 0; i < LOOP; i++) 
	{

		sem_wait(&empty);
		while (fifo->full) 
		{
		}

		sem_wait(&mutex);
		queueAdd (fifo, i+1);
		sem_post(&mutex);
		sem_post(&full);
		printf ("producer: produced %d th.\n",i+1);

		/* sleep */
		usleep ( PRODUCER_SLEEP_S * 1000000); 

		/*******Release the locks**********/

	}

	return (NULL);
}

/***** consumer *****/
void *consumer (void *q)
{
	queue *fifo;
	int d;

	fifo = (queue *)q;

	/* Simulate Consumers' random arrvial */
	usleep ( (rand()%LOOP) * 1000000); 


	/**
	  obtain the lock and release the lock somewhere
	 **/

	sem_wait(&full);
	while (fifo->empty) 
	{
	}
	sem_wait(&mutex);

	/* sleep */
	usleep ( CONSUMER_SLEEP_S * 1000000); 

	queueDel (fifo, &d);
	sem_post(&mutex);
	sem_post(&empty);
	printf ("------------------------------------>consumer: recieved %d.\n", d);		

	return (NULL);
}


/***** queueInit *****/
queue *queueInit (void)
{
	queue *q;
	int i;

	q = (queue *)malloc (sizeof (queue));
	if (q == NULL) return (NULL);

	for(i=0;i<QUEUESIZE;i++)
	{
		q->buf[i]=0;
	}
	q->empty = 1;
	q->full = 0;
	q->head = 0;
	q->tail = 0;

	/*Initialize the locks here	*/


	return (q);
}


/***** queueDelete *****/
void queueDelete (queue *q)
{

	/* free the locks here*/


	/* free memory used for queue */
	free (q);
}

/***** queueAdd *****/
void queueAdd (queue *q, int in)
{
	q->buf[q->tail] = in;
	q->tail++;
	if (q->tail == QUEUESIZE)
		q->tail = 0;
	if (q->tail == q->head)
		q->full = 1;
	q->empty = 0;

	return;
}

/***** queueDel *****/
void queueDel (queue *q, int *out)
{
	*out = q->buf[q->head];
	q->buf[q->head]=0;

	q->head++;

	if (q->head == QUEUESIZE)
		q->head = 0;
	if (q->head == q->tail)
		q->empty = 1;
	q->full = 0;

	return;
}
