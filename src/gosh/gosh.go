package main

import (
	"bufio"
	"container/list"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"parse"
	"runtime"
	"strings"
)

var jobLock bool //used for locking the list of jobs

// builds a prompt
// string is a first-class type in go, so we just return one
func buildPrompt() string {
	// Go supports multiple return values.
	// := instantiates variables with implied type
	wd, err := os.Getwd()
	if err != nil {
		// err is not nil if there is an error. Go convention
		return "[error getting current dir] $ "
	}
	return wd + " $ "
}

// this is an interesting thing not to be in the standard library
// the _ is an anonymous variable - I'm saying I'm ignoring two of the returned variables
// this would be bad, but I deal with the error case (empty return) elsewhere.
func readline() string {
	r := bufio.NewReader(os.Stdin)
	line, _, _ := r.ReadLine()
	return string(line)
}

// execcmd takes our parsed command line, knowing it is a command in the path, and runs it
// returns the cmd object (which contains such things as *os.Process
// and the error, if there is one
func execcmd(info parse.Parseinfo, jobs *list.List) error {
	cmd := exec.Command(info.Varlist[0], info.Varlist[1:]...)
	cmd.Stderr = os.Stderr
	if len(info.Outfile) != 0 {
		// redirection of stdout
		file, err := os.Create(info.Outfile)
		if err != nil {
			return err
		}
		defer file.Close()
		cmd.Stdout = file
	} else {
		cmd.Stdout = os.Stdout
	}
	if len(info.Infile) != 0 {
		// redirection of stdin
		file, err := os.Open(info.Infile)
		if err != nil {
			return err
		}
		defer file.Close()
		cmd.Stdin = file
	} else {
		cmd.Stdin = os.Stdin
	}
	var err error
	if info.Background {
		// run job in background
		for jobLock != false {
			// wait for jobs to unlock
		}
		jobLock = true
		jobs.PushBack(cmd)
		jobLock = false
		err = cmd.Start()
		// go runs an expression asynchronosly
		go func(e *list.Element) { //oh look, a closure
			cmd.Wait() //wait for the command to finish
			for jobLock != false {
			}
			jobLock = true
			jobs.Remove(e) //then remove it from the list of jobs
			jobLock = false
		}(jobs.Back())
	} else {
		// run job in foregroun
		err = cmd.Run()
	}
	return err
}

// do takes our parsed command line, deals with builtins, and then lets execcmd deal with actual work
func do(info parse.Parseinfo, hist *[]parse.Parseinfo, jobs *list.List) error {
	//fmt.Printf("%+v\n", info)
	*hist = append(*hist, info)
	c := info.Varlist[0]
	if strings.HasPrefix(c, "!") {
		return runHistCommand(info, hist, jobs)
	}
	if c == "history" {
		printHist(*hist)
		return nil
	}
	if c == "exit" {
		if jobs.Len() == 0 {
			os.Exit(0)
		} else {
			return errors.New("there are running jobs")
		}
	}
	if c == "cd" {
		return cd(info)
	}
	if c == "jobs" {
		printJobs(jobs)
		return nil
	}
	if c == "kill" {
		return kill(info, jobs)
	}
	if c == "help" {
		printHelp()
		return nil
	}
	//okay, not a builtin - time to run it
	return execcmd(info, jobs)
}

// main is where the point of entry is
// executables are always in package main
func main() {
	runtime.GOMAXPROCS(runtime.NumCPU()) // set the number of CPUs goroutines can use to all of them
	var hist []parse.Parseinfo           // hold history as a slice of parsed command lines
	jobs := list.New()                   // jobs is going to be a linked list
	jobLock = false                      // don't want to lock jobs yet!
	for {
		fmt.Printf(buildPrompt())                // equivalent to printf()
		info, err := parse.Parseline(readline()) // read line and parse it
		if err != nil {
			if err.Error() != "parse: empty command" {
				fmt.Println(err)
			}
			continue
		}
		err = do(info, &hist, jobs) // now go and do!
		if err != nil {
			fmt.Println(err)
		}
	}
}
