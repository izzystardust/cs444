package main

import (
	"container/list"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"parse"
	"strconv"
	"strings"
)

// print the history
func printHist(hist []parse.Parseinfo) {
	for i := 0; i < len(hist); i++ {
		fmt.Printf("%3d: %s\n", i+1, hist[i].Cmd)
	}
}

// print backgrounded jobs
func printJobs(jobs *list.List) {
	i := 1
	jobLock = true // don't want job list to change during printing!
	for e := jobs.Front(); e != nil; e = e.Next() {
		a := e.Value.(*exec.Cmd)
		fmt.Printf("%3d %s\n", i, a.Args)
		i++
	}
	jobLock = false
}

// print help
func printHelp() {
	fmt.Println("cd [dir]: set current working directory")
	fmt.Println("jobs    : list running jobs")
	fmt.Println("history : print history")
	fmt.Println("![n]    : n > 0: run command from hist with that number")
	fmt.Println("          n < 0: same, but count from the bottom")
	fmt.Println("kill n  : kill job numbered n")
	fmt.Println("exit    : terminate gosh")
	fmt.Println("help    : print this help text")
}

// run a command from history
func runHistCommand(info parse.Parseinfo, hist *[]parse.Parseinfo, jobs *list.List) error {
	i, err := strconv.Atoi((strings.TrimPrefix(info.Cmd, "!"))) // gets the number out of !n
	if err != nil {
		return err
	}

	if i == 0 {
		return errors.New("You're funny.")
	}

	var todo parse.Parseinfo
	if i > 0 {
		// positive case
		if i > len(*hist)-1 {
			return errors.New("History index out of bounds")
		}
		todo = (*hist)[i-1]
	} else {
		// negative case
		i--
		if len(*hist)+i < 0 {
			return errors.New("History index out of bounds")
		}
		todo = (*hist)[len(*hist)+i]
	}

	if todo.Cmd[0] == "!"[0] {
		// wouldn't be a problem if instead of adding !n to history,
		// the command that was run was added instead.
		return errors.New("No history recursion allowed!")
	}
	err = do(todo, hist, jobs)
	return err
}

// fairly self explanatory
func cd(info parse.Parseinfo) error {
	target := os.ExpandEnv("$HOME") // cd with no args goes to ~
	if len(info.Varlist) != 1 {
		target = os.ExpandEnv(strings.Replace(info.Varlist[1], "~", "$HOME", -1)) // replace ~ with abs
	}
	return os.Chdir(target)
}

func killAll(jobs *list.List) {
	jobLock = true
	for e := jobs.Front(); e != nil; e = e.Next() {
		e.Value.(*exec.Cmd).Process.Kill()
	}
	jobLock = false
}

func kill(info parse.Parseinfo, jobs *list.List) error {
	if len(info.Varlist) != 2 {
		return errors.New("kill: syntax error")
	}
	if info.Varlist[1] == "*" {
		killAll(jobs)
		return nil
	}
	target, err := strconv.Atoi(info.Varlist[1])
	if err != nil {
		return errors.New("kill: syntax error")
	}
	jobLock = true //if we are iterating through and the list changed...
	e := jobs.Front()
	i := 1
	if (target > jobs.Len()) {
		return errors.New("kill: no such jobs")
	}
	for i < target {
		i++
		e = e.Next()
	}
	e.Value.(*exec.Cmd).Process.Kill() // he's dead, jim
	jobLock = false
	// we don't have to worry about removing it from the jobs list
	// because the goroutine waiting on the job to end will no longer be blocking
	return nil
}
