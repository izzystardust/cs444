CS444
Shell Assignment
Ethan Miller; 0278718

Code status: complete
Known bugs: none

This assignment has been completed in Go (http://www.golang.org)
Go is a garbage-collected, modern language initially developed by Google.

To run the shell:
0. Install the go tools. On Arch, the package is "go"
1. Untar this archive somewhere. I'll refer to that spot as $SPOT
2. Add that spot to your GOPATH (ie, export GOPATH=$SPOT)
3. cd $SPOT/src/shell
4. go run shell.go builtins.go

Step 4 will compile and run the shell.

Completed features:
1. 
